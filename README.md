## Conversie algoritme ##

### Setup ###
* Create an empty database
* Change database credientials in huwebshop/settings.py
* Install dependencies
    * psycopg2
    * pymongo
    * django
    * MongoHandler

### Run migrations ###
* `$ cd huwebshop`
* `python manage.py makemigrations`
* `python manage.py migrate huengine 0001`
