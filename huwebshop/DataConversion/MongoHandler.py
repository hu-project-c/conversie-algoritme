from pymongo import MongoClient
class MongoHandler:
    def getData(self, table_name):
        """ Fetches data from proivded database, as dictionary.
        input: database name (string)
        output: dictionary (key: product_id, value: properties"""

        client = MongoClient()
        with client.start_session() as session:
            collection = client['huwebshop']

            cur = collection[table_name].find({}, no_cursor_timeout=True, session=session)

            dictionary = {}
            count = 0
            for data in cur:
                client.admin.command(
                    'refreshSessions', [session.session_id], session=session)
                data2 = data.copy()
                data2.pop("_id")
                dictionary[data["_id"]] = data2
                count += 1

            cur.close()
            return dictionary

