import uuid
import psycopg2


class PostgresHandler:

    tupleListRecommendationVbf = []
    tupleListVbfProducts = []
    tupleListSimilars = []
    tupleListSimilarsProducts = []
    tupleListSessionBuids = []

    def __init__(self):
        """
        Constructor function
        """
        self.password = "gc6501"  # change this to your password

    def writeData(self, sqlString, sql):
        """
        Writes an SQL query to the database.
        :param silent :
        :param  sqlString:
        :param sql :
        :return void
        """

        try:
            conn = psycopg2.connect(f"dbname=analyse_database user=postgres password={self.password}",
                                    connect_timeout=1000000000)
            cur = conn.cursor()

            cur.executemany(sqlString, sql)
            conn.commit()
            # print("DONE!")
            cur.close()
            conn.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def insertDataProducts(self, products):
        """
        Function inserts all data from products
        :param products:
        :return void:
        """

        # Query for products
        sqlStringProducts = "INSERT INTO huengine_product(id, brand, category, color, deeplink, description, fastmover, " \
                            "flavor, gender, herhaalaankopen, name, predict_out_of_stock, recommendable, size, " \
                            "sub_category, sub_sub_category, price_id) " \
                            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        # Query for price
        sqlStringPrice = "INSERT INTO huengine_price(id, discount, mrsp, selling_price)" \
                         "VALUES(%s, %s, %s, %s)"

        # Query for properties
        sqlStringProperties = "INSERT INTO huengine_property(id, name, value, product_id)" \
                              "VALUES(%s, %s, %s, %s)"
        # Query for stock
        sqlStringStock = "INSERT INTO huengine_stock(id, date, stock_level, product_id)" \
                         "VALUES(%s, %s, %s, %s)"

        # Lists with tuples
        tupleListProduct = []
        tupleListPrice = []
        tupleListProperties = []
        tupleListStock = []

        # Loop through all products
        for product_id in products:

            # Dictionary to set all keys
            valueDict = {"id": product_id, "brand": "", "category": "", "color": "", "deeplink": "", "description": "",
                         "fast_mover": "", "flavor": "", "gender": "", "herhaalaankopen": "", "name": "",
                         "predict_out_of_stock_date": "", "recommendable": "", "size": "", "sub_category": "",
                         "sub_sub_category": "", "price_id": ""}

            # Loop trough all attributes from a product
            for attribute in products[product_id]:
                if attribute == "price":
                    # Generate unique id for price
                    newUuid = uuid.uuid4()

                    # Set price id
                    valueDict["price_id"] = str(newUuid)

                    # Set price and add to tuple list
                    tupleListPrice.append(self.insertPrice(str(newUuid), products[product_id][attribute]))

                elif attribute == "properties":

                    propertiesDict = products[product_id][attribute]
                    for property in propertiesDict:
                        # Set and generate property id
                        newUuid = uuid.uuid4()
                        id = str(newUuid)
                        # Convert data to writeable data
                        tupleListProperties.append(
                            self.insertProperty(id, property, propertiesDict[property], product_id))

                elif attribute == "stock":
                    # Value of attribute
                    attributeValue = products[product_id][attribute]

                    for stock in attributeValue:
                        # Set and generate stock id
                        newUuid = uuid.uuid4()
                        try:
                            # convert data to writable data.
                            tupleListStock.append(self.insertStock(str(newUuid),
                                                                   stock['date'],
                                                                   stock['stock_level'],
                                                                   product_id))
                        except TypeError:
                            # 3 data points have no date, therefore they are ignored.
                            pass
                elif attribute == "sm":
                    pass
                elif attribute == "sub_sub_sub_category":
                    pass
                elif attribute == "rivals":
                    pass
                elif attribute == "product":
                    pass
                else:
                    attributeValue = products[product_id][attribute]

                    # Loop through all keys from value dict
                    for key in valueDict:
                        if key == attribute:
                            valueDict[key] = attributeValue

            # Loop through our value dict and set empty values to none
            for key in valueDict:
                if not valueDict[key] and valueDict[key] is not None:
                    valueDict[key] = None
            tupleListProduct.append(dictToTuple(valueDict))

        # Write data to database
        self.writeData(sqlStringPrice, tupleListPrice)
        self.writeData(sqlStringProducts, tupleListProduct)
        self.writeData(sqlStringProperties, tupleListProperties)
        self.writeData(sqlStringStock, tupleListStock)

    def insertPrice(self, uuid, priceList):
        """
        Function to convert the data to dictonary, to format, then to list so its writeable (for postgres)
        :param uuid:
        :param priceList:
        :return tuple
        """

        valueDict = {"id": str(uuid), "discount": 0, "mrsp": 0, "selling_price": 0}

        for key in valueDict:
            for priceKey in priceList:
                if priceKey == key:
                    valueDict[key] = priceList[priceKey]

        for key in valueDict:
            if not valueDict[key]:
                valueDict[key] = None

        return dictToTuple(valueDict)

    def insertProperty(self, uuid, name, value, product_id):
        """
        Function to convert the data to dictonary, to format, then to list so its writeable (for postgres)
        :param uuid:
        :param id:
        :param name:
        :param value:
        :param product_id:
        :return tuple
        """

        valueDict = {"id": uuid, "name": name, "value": value, "product_id": product_id}

        return dictToTuple(valueDict)

    def insertStock(self, id, date, stock_level, product_id):
        """
        Function to convert the data to dictonary, to format, then to list so its writeable (for postgres)
        :param string id:
        :param string date:
        :param string stock_level:
        :param string product_id:
        :return tuple
        """

        valueDict = {"id": id, "date": date, "stock_level": stock_level, "product_id": product_id}
        return dictToTuple(valueDict)

    def insertDataProfiles(self, profiles):
        """
        Insert all profile data
        :param profiles:
        :return void:
        """

        # Query for profile
        sqlStringProfile = "INSERT INTO huengine_profile(id, unique_hash, latest_activity)" \
                           "VALUES(%s, %s, %s)"

        # Query for Recommendations
        sqlStringRecommendations = "INSERT INTO huengine_recommendation (id, timestamp, segment, latest_visit, total_pageview_count, total_viewed_count, profile_id)" \
                                    "VALUES (%s, %s, %s, %s, %s, %s, %s)"

        # Query for profile products
        sqlStringProfileProducts = "INSERT INTO huengine_profile_products(profile_id, product_id)" \
                           "VALUES(%s, %s)"

        # Sql string viewed before
        sqlStringViewedBefore = "INSERT INTO huengine_viewedbefore(id, recommendation_id)" \
                                "VALUES(%s, %s)"

        # Sql string viewed before products
        sqlStringViewedBeforeProducts = "INSERT INTO huengine_viewedbefore_products(viewedbefore_id, product_id)" \
                                        "VALUES(%s, %s)"

        # Sql string similars
        sqlStringSimilars = "INSERT INTO huengine_similars(id, recommendation_id)" \
                            "VALUES(%s, %s)"

        # Sql string similars products
        sqlStringSimilarsProducts = "INSERT INTO huengine_similars_products(similars_id, product_id)" \
                                    "VALUES(%s, %s)"

        # Tuple lists Profiles and Recommendations
        tupleListProfile = []
        tupleListRecommendation = []
        tupleListProfileBuids = []
        valueDictProfileProducts = []

        for profile_id in profiles:
            # Dictionary to set all keys (temporarily unique_hash = Fasle due to constraint)
            valueDictProfiles = {'id': str(profile_id), 'unique_hash': False, 'latest_activity': None}

            # Recommendation id
            for attribute in profiles[profile_id]:
                # Value of attribute
                attributeValue = profiles[profile_id][attribute]

                if attribute == "buids":
                    if attributeValue:
                        for buid in attributeValue:
                            valueDictBuids = {'id': str(profile_id), 'buid': buid}
                            tupleListProfileBuids.append(dictToTuple(valueDictBuids))

                elif attribute == "previously_recommended":
                    if attributeValue:
                        for product_id in attributeValue:
                            valueDictProductID = (str(profile_id), product_id)
                            valueDictProfileProducts.append(valueDictProductID)
                elif attribute == "recommendations":
                    valueDictRecommendations = self.insertRecommendations(profile_id, attributeValue)
                    tupleListRecommendation.append(dictToTuple(valueDictRecommendations))
                elif attribute == "order":
                    if attributeValue:
                        for value in attributeValue:
                            if value == "ids":
                                for id in attributeValue[value]:
                                    valueDictProfileProducts.append((str(profile_id), str(id)),)
                elif attribute == "meta":
                    pass
                elif attribute == "sm":
                    pass
                else:
                    for key in valueDictProfiles:
                        if attribute == key:
                            valueDictProfiles[key] = str(attributeValue)
            tupleListProfile.append(dictToTuple(valueDictProfiles))

        # Write data
        self.writeData(sqlStringProfile, tupleListProfile)
        self.writeData(sqlStringRecommendations, tupleListRecommendation)
        self.writeData(sqlStringProfileProducts, valueDictProfileProducts)
        self.insertProfileSessions(tupleListProfileBuids)

        # Insert recommendation splits to database
        self.writeData(sqlStringViewedBefore, self.tupleListRecommendationVbf)
        self.writeData(sqlStringViewedBeforeProducts, self.tupleListVbfProducts)
        self.writeData(sqlStringSimilars, self.tupleListSimilars)
        self.writeData(sqlStringSimilarsProducts, self.tupleListSimilarsProducts)

    def insertRecommendations(self, profile_id, recommendationData):
        """
        Insert recommendation data to the database
        :param profile_id:
        :param recommendationData:
        :return dict:
        """

        # Generate uuid for the recommendation
        id = str(uuid.uuid4())

        # Dict for a recommendation
        valueDictRecommendations = {'id': id, 'timestamp': None, 'segment': None, 'latest_visit': None,
                                    'total_pageview_count': None, 'total_viewed_count': None,
                                    'profile_id': str(profile_id)}

        # Tuple lists
        self.tupleListRecommendationVbf = []
        self.tupleListVbfProducts = []
        self.tupleListSimilars = []
        self.tupleListSimilarsProducts = []

        for keyDict in valueDictRecommendations:
            for keyData in recommendationData:
                if keyDict == keyData:
                    valueDictRecommendations[keyDict] = recommendationData[keyData]
                elif keyData == "viewed_before":
                    if recommendationData[keyData]:
                        for key in recommendationData[keyData]:
                            # generate viewedbefore_id
                            vbfId = str(uuid.uuid4())
                            # add relation recommendation - viewed before
                            self.tupleListRecommendationVbf.append(self.insertViewedBefore(vbfId, id))
                            # add relation viewedbefore - products
                            self.tupleListVbfProducts.append(self.instertViewBeforeProducts(vbfId, key))
                elif keyData == "similars":
                    if recommendationData[keyData]:
                        for similar in recommendationData[keyData]:
                            # generate similar_id
                            similarId = str(uuid.uuid4())

                            # add relation similars - recommendation
                            self.tupleListSimilars.append(self.instertSimilars(similarId, id))

                            # add relation similars - products
                            self.tupleListSimilarsProducts.append(self.instertSimilarsProducts(similarId, similar))
        return valueDictRecommendations

    def insertProfileSessions(self, profileBuids):
        """
        Generate relation for profile sessions.
        :param profileBuids:
        :return void:
        """

        # Sql string profile session
        sqlStringProfileSession = "INSERT INTO huengine_profile_sessions(profile_id, session_id)" \
                                    "VALUES(%s, %s)"

        # Tuple list for sessions
        tupleListProfileSessions = []

        for profileBuid in profileBuids:
            for sessionBuid in self.tupleListSessionBuids:
                if profileBuid and sessionBuid:
                    if len(profileBuid) == 2 and len(sessionBuid) == 2:
                        if profileBuid[1] == sessionBuid[1]:
                            tupleListProfileSessions.append((profileBuid[0], sessionBuid[0]))
        self.writeData(sqlStringProfileSession, tupleListProfileSessions)

    def insertViewedBefore(self, id, recommendation_id):
        """
        :param string id:
        :param string recommendation_id:
        :return tuple:
        """
        valueDict = {'id': id, 'recommendation_id': recommendation_id}
        return dictToTuple(valueDict)

    def instertViewBeforeProducts(self, vbf_id, product_id):
        """
        :param vbf_id:
        :param product_id:
        :return tuple:
        """
        valueDict = {'vbf_id': vbf_id, 'product_id': product_id}
        return dictToTuple(valueDict)

    def instertSimilars(self, similar_id, recommendation_id):
        """
        :param similar_id:
        :param recommendation_id:
        :return tuple:
        """
        valueDict = {'id': similar_id, 'recommendation_id': recommendation_id}
        return dictToTuple(valueDict)

    def instertSimilarsProducts(self, similars_id, product_id):
        """
        :param similars_id :
        :param product_id:
        :return tuple:
        """

        valueDict = {'similars_id': similars_id, 'product_id': product_id}
        return dictToTuple(valueDict)

    def insertDataSessions(self, sessions):
        """
        Insert session data and split between tables.
        :param sessions:
        :return void:
        """

        # Query for session
        sqlStringSession = "INSERT INTO huengine_session(id, session_start, session_end, has_sale)" \
                           "VALUES(%s, %s, %s, %s)"

        # Query for events
        sqlStringEvents = "INSERT INTO huengine_event(id, time, source, action, pagetype, time_on_page, max_time_inactive, click_count, elements_clicked, scrolls_down, scrolls_up, product_id, session_id)" \
                           "VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        # Query for orders
        sqlStringOrders = "INSERT INTO huengine_order(id, session_id) VALUES(%s, %s)"

        # Query for oder products
        sqlStringOrderProducts = "INSERT INTO huengine_orders_products_count(order_id, product_id, product_amount) " \
                                 "VALUES(%s, %s, %s)"

        # Tuple lists
        tupleListSession = []
        tupleListEvents = []
        tupleListOrder = []
        tupleListOrderProducts = []
        tupleListOrderProductsCount = []

        for session_id in sessions:
            # Dictionary to set all keys
            valueDict = {'id': session_id, 'session_start': None, 'session_end': None, 'has_sale': None}

            for attribute in sessions[session_id]:
                # Value of attribute
                attributeValue = sessions[session_id][attribute]

                if attribute == "buid":
                    if attributeValue:
                        for buid in attributeValue:
                                self.tupleListSessionBuids.append(buid)
                elif attribute == "order" and attributeValue:

                    # Generate uuid for order
                    order_id = str(uuid.uuid4())

                    # Append order to tuple
                    tupleListOrder.append((order_id, str(session_id)))

                    # Loop trough order attributes
                    for order_attribute in attributeValue:
                        if order_attribute == 'products':
                            for product in attributeValue[order_attribute]:
                                tupleListOrderProducts.append((str(order_id), product['id']))
                elif attribute == "events":

                    # Generate uuid for event
                    event_id = str(uuid.uuid4())

                    # Add event to tuple list
                    tupleListEvents.append(self.insertEvent(event_id, sessions[session_id][attribute], session_id))
                elif attribute == "sources":
                    pass
                elif attribute == "preferences":
                    pass
                elif attribute == "cg_treated":
                    pass
                elif attribute == "tg_treated":
                    pass
                elif attribute == "segment":
                    pass
                elif attribute == "user_agent":
                    pass
                else:
                    for key in valueDict:
                        if key == attribute:
                            valueDict[key] = str(attributeValue)
            tupleListSession.append(dictToTuple(valueDict))

        # Write data to database
        self.writeData(sqlStringSession, tupleListSession)
        self.writeData(sqlStringEvents, tupleListEvents)
        self.writeData(sqlStringOrders, tupleListOrder)

        # Loop through order products and add new tuple with product amount
        for item in tupleListOrderProducts:
            newTuple = countOrderProducts(tupleListOrderProducts, item)
            if newTuple not in tupleListOrderProductsCount:
                tupleListOrderProductsCount.append(newTuple)

        self.writeData(sqlStringOrderProducts, tupleListOrderProductsCount)

    def insertEvent(self, uuid , events, session_id):
        """
        Insert event
        :param uuid:
        :param events:
        :param session_id:
        :return tuple:
        """
        valueDictEvents = {'id': uuid, 'time': None, 'source': None, 'action': None, 'pagetype': None,
                           'time_on_page': None, 'max_time_inactive': None, 'click_count': None,
                           'elements_clicked': None, 'scrolls_down': None, 'scrolls_up': None, 'product_id': None,
                           'session_id': str(session_id)}

        for event in events:
            for eventAttribute in event:
                for key in valueDictEvents:
                    if key == eventAttribute:
                        valueDictEvents[eventAttribute] = event[eventAttribute]
        return dictToTuple(valueDictEvents)

    def insertOrder(self, orders):
        pass


def dictToTuple(dict):
    """
    Function to convert all values from dictonary to a tuple
    :param dict:
    :return tuple:
    """
    values = ()

    for key in dict:
        keyValue = (dict[key],)
        values += keyValue
    return values

def countOrderProducts(orderProductsList, orderProductInput):
    """
    Count acmount of same products in an order
    :param orderProductsList:
    :return tuple:
    """

    counter = 0
    for orderProduct in orderProductsList:
        if orderProduct[1] == orderProductInput[1]:
            counter += 1

    newTuple = (orderProductInput[0], orderProductInput[1], counter)
    return newTuple