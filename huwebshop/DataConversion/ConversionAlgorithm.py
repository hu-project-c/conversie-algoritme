from MongoHandler import MongoHandler
from PostgresHandler import PostgresHandler
from itertools import islice
import multiprocessing
import time

class DatabaseConverter:

    def start(self, chuncks, type):

        # Handler for postgresql
        postgres = PostgresHandler()

        processPool = []

        for chunck in chuncks:
            if type == "product":
                process = multiprocessing.Process(target=postgres.insertDataProducts(chunck))
                process.start()
                processPool.append(process)
            elif type == "session":
                process = multiprocessing.Process(target=postgres.insertDataSessions(chunck))
                process.start()
                processPool.append(process)
            elif type == "profile":
                process = multiprocessing.Process(target=postgres.insertDataProfiles(chunck))
                process.start()
                processPool.append(process)

        return processPool

    def chunks(self, data, size):
        it = iter(data)
        for i in range(0, len(data), size):
            yield {k: data[k] for k in islice(it, size)}

    def convertData(self):
        """ Main function for data conversion, fetches the data from mongodb, then adds it to postgreSQL db."""

        start = time.time()

        # Get products from dataset
        products = MongoHandler().getData("products")
        productChuncks = list(self.chunks(products, 1000))

        # Get sessions from dataset
        sessions = MongoHandler().getData("sessions")
        sessionChuncks = list(self.chunks(sessions, 10000))

        # Get profiles from dataset
        profiles = MongoHandler().getData("profiles")
        profileChuncks = list(self.chunks(profiles, 10000))

        # Pool of processes
        self.start(productChuncks, "product"),
        self.start(sessionChuncks, "session"),
        self.start(profileChuncks, "profile")

        end = time.time()
        print(f"All done in exactly {round(end - start)} seconds!")

if __name__ == '__main__':
    DatabaseConverter().convertData()
