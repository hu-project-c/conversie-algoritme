# Generated by Django 3.1.7 on 2021-03-08 13:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('discount', models.IntegerField(null=True)),
                ('mrsp', models.IntegerField(null=True)),
                ('selling_price', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('brand', models.CharField(max_length=1000, null=True)),
                ('category', models.CharField(max_length=1000, null=True)),
                ('color', models.CharField(max_length=1000, null=True)),
                ('deeplink', models.CharField(max_length=1000, null=True)),
                ('description', models.CharField(max_length=1000, null=True)),
                ('fastmover', models.BooleanField(null=True)),
                ('flavor', models.CharField(max_length=1000, null=True)),
                ('gender', models.CharField(max_length=1000, null=True)),
                ('herhaalaankopen', models.BooleanField(null=True)),
                ('name', models.CharField(max_length=1000, null=True)),
                ('predict_out_of_stock', models.CharField(max_length=1000, null=True)),
                ('recommendable', models.BooleanField(null=True)),
                ('size', models.CharField(max_length=1000, null=True)),
                ('sub_category', models.CharField(max_length=1000, null=True)),
                ('sub_sub_category', models.CharField(max_length=1000, null=True)),
                ('price', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='huengine.price')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('unique_hash', models.BooleanField(null=True)),
                ('latest_activity', models.DateTimeField(null=True)),
                ('products', models.ManyToManyField(to='huengine.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Recommendation',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('timestamp', models.DateTimeField(null=True)),
                ('segment', models.CharField(max_length=1000, null=True)),
                ('latest_visit', models.DateTimeField(null=True)),
                ('total_pageview_count', models.IntegerField(null=True)),
                ('total_viewed_count', models.IntegerField(null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.profile')),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('session_start', models.DateTimeField(null=True)),
                ('session_end', models.DateTimeField(null=True)),
                ('has_sale', models.BooleanField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ViewedBefore',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('products', models.ManyToManyField(to='huengine.Product')),
                ('recommendation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.recommendation')),
            ],
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('date', models.CharField(max_length=1000, null=True)),
                ('stock_level', models.IntegerField(null=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.product')),
            ],
        ),
        migrations.CreateModel(
            name='Similars',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('products', models.ManyToManyField(to='huengine.Product')),
                ('recommendation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.recommendation')),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1000, null=True)),
                ('value', models.CharField(max_length=1000, null=True)),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='huengine.product')),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='sessions',
            field=models.ManyToManyField(to='huengine.Session'),
        ),
        migrations.CreateModel(
            name='orders_products_count',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_amount', models.IntegerField(default=1)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.order')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.product')),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(through='huengine.orders_products_count', to='huengine.Product'),
        ),
        migrations.AddField(
            model_name='order',
            name='session',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='huengine.session'),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.CharField(max_length=1000, primary_key=True, serialize=False)),
                ('time', models.DateTimeField(null=True)),
                ('source', models.CharField(max_length=1000, null=True)),
                ('action', models.CharField(max_length=1000, null=True)),
                ('pagetype', models.CharField(max_length=1000, null=True)),
                ('time_on_page', models.IntegerField(null=True)),
                ('max_time_inactive', models.IntegerField(null=True)),
                ('click_count', models.IntegerField(null=True)),
                ('elements_clicked', models.IntegerField(null=True)),
                ('scrolls_down', models.IntegerField(null=True)),
                ('scrolls_up', models.IntegerField(null=True)),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='huengine.product')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='huengine.session')),
            ],
        ),
    ]
