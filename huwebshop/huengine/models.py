from django.db import models

class Price(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    discount = models.IntegerField(null=True)
    mrsp = models.IntegerField(null=True)
    selling_price = models.IntegerField(null=True)

class Session(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    session_start = models.DateTimeField(null=True)
    session_end = models.DateTimeField(null=True)
    has_sale = models.BooleanField(null=True)

class Product(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    brand = models.CharField(max_length=1000, null=True)
    category = models.CharField(max_length=1000, null=True)
    color = models.CharField(max_length=1000, null=True)
    deeplink = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=1000, null=True)
    fastmover = models.BooleanField(null=True)
    flavor = models.CharField(max_length=1000, null=True)
    gender = models.CharField(max_length=1000, null=True)
    herhaalaankopen = models.BooleanField(null=True)
    name = models.CharField(max_length=1000, null=True)
    predict_out_of_stock = models.CharField(max_length=1000, null=True)
    recommendable = models.BooleanField(null=True)
    size = models.CharField(max_length=1000, null=True)
    sub_category = models.CharField(max_length=1000, null=True)
    sub_sub_category = models.CharField(max_length=1000, null=True)

    # Relations

    # One to one
    price = models.ForeignKey(
        Price,
        on_delete=models.CASCADE,
        null=True
    )

class Profile(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    unique_hash = models.BooleanField(null=True)
    latest_activity = models.DateTimeField(null=True)

    # Relations

    # Many to many
    sessions = models.ManyToManyField(Session, )
    products = models.ManyToManyField(Product, )

class Event(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    time = models.DateTimeField(null=True)
    source = models.CharField(max_length=1000, null=True)
    action = models.CharField(max_length=1000, null=True)
    pagetype = models.CharField(max_length=1000, null=True)
    time_on_page = models.IntegerField(null=True)
    max_time_inactive = models.IntegerField(null=True)
    click_count = models.IntegerField(null=True)
    elements_clicked = models.IntegerField(null=True)
    scrolls_down = models.IntegerField(null=True)
    scrolls_up = models.IntegerField(null=True)

    # Relations

    # Many to one
    session = models.ForeignKey(
        Session,
        on_delete=models.CASCADE,
        
    )

    # Many to one
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        null=True,
        
    )

class Recommendation(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    timestamp = models.DateTimeField(null=True)
    segment = models.CharField(max_length=1000, null=True)
    latest_visit = models.DateTimeField(null=True)
    total_pageview_count = models.IntegerField(null=True)
    total_viewed_count = models.IntegerField(null=True)

    # Relations
    # Many to one
    profile = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        
    )

class ViewedBefore(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    # Relations

    # One to one
    recommendation = models.ForeignKey(
        Recommendation,
        on_delete=models.CASCADE,
        
    )

    # Many to many
    products = models.ManyToManyField(Product, )

class Similars(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    # Relations

    # One to one
    recommendation = models.ForeignKey(
        Recommendation,
        on_delete=models.CASCADE,
        
    )

    # Many to many
    products = models.ManyToManyField(Product, )


class Property(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    name = models.CharField(max_length=1000, null=True)
    value = models.CharField(max_length=1000, null=True)

    # Relations

    # Many to one
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        null=True,
        
    )

class Stock(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)
    date = models.CharField(max_length=1000, null=True)
    stock_level = models.IntegerField(null=True)
    # Relations

    # Many to one
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        
    )

class Order(models.Model):
    id = models.CharField(primary_key=True, max_length=1000)

    # Relations
    products = models.ManyToManyField(Product, through='orders_products_count')

    # Many to one
    session = models.ForeignKey(
        Session,
        on_delete=models.CASCADE,
        null=True,
    )

class orders_products_count(models.Model):
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE
    )

    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE
    )

    product_amount = models.IntegerField(default=1)



